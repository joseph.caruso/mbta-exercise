package com.caruso.mbta.exercise.api;

import com.caruso.mbta.exercise.handler.HttpClient;
import com.caruso.mbta.exercise.handler.HttpException;
import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import junit.framework.TestCase;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.impl.io.EmptyInputStream;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.io.InputStream;
import java.util.jar.JarException;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(DataProviderRunner.class)
public class MbtaApiImplTest {

    private MbtaApiImpl classUnderTest = mock(MbtaApiImpl.class);


    @DataProvider
    public static Object[][] encodeTestData(){
        return new Object[][] {
                {"/", "%2F"},
                {" ", "+"},
                {"\\", "%5C"},
                {"-", "-"},
                {"=", "%3D"},
                {"`", "%60"}
        };
    }

    @Test
    @UseDataProvider("encodeTestData")
    public void testEncodeValue(String input, String expectedOutput) {
        when(classUnderTest.encodeValue(input)).thenCallRealMethod();

        String encodedValue = classUnderTest.encodeValue(input);

        Assert.assertEquals(expectedOutput, encodedValue);
    }

    @Test(expected = HttpException.class)
    public void testConvertHttpEntityToJsonEntityContentThrowsJSONException() throws IOException {

        // ensure getContentType() does not return null
        Header header = mock(Header.class);

        HttpEntity entity = mock(HttpEntity.class);
        when(entity.getContentType()).thenReturn(header);
        when(entity.getContent()).thenThrow(new JSONException(""));

        HttpResponse response = mock(HttpResponse.class);
        when(response.getEntity()).thenReturn(entity);

        StatusLine statusLine = mock(StatusLine.class);
        when(statusLine.getStatusCode()).thenReturn(404);

        when(response.getStatusLine()).thenReturn(statusLine);

        when(classUnderTest.convertHttpEntityToJson(response)).thenCallRealMethod();

        classUnderTest.convertHttpEntityToJson(response);
    }

    @Test(expected = HttpException.class)
    public void testConvertHttpEntityToJsonEntityContentThrowsJSONExceptionWithNullStatusLine() throws IOException {

        // ensure getContentType() does not return null
        Header header = mock(Header.class);

        HttpEntity entity = mock(HttpEntity.class);
        when(entity.getContentType()).thenReturn(header);
        when(entity.getContent()).thenThrow(new JSONException(""));

        HttpResponse response = mock(HttpResponse.class);
        when(response.getEntity()).thenReturn(entity);

        when(response.getStatusLine()).thenReturn(null);

        when(classUnderTest.convertHttpEntityToJson(response)).thenCallRealMethod();

        classUnderTest.convertHttpEntityToJson(response);
    }

    @Test
    public void testConvertHttpEntityToJsonEntityContentIsEmptyStream() throws IOException {
        EmptyInputStream inputStream = mock(EmptyInputStream.class);
        //when(inputStream.getClass()).thenReturn(EmptyInputStream.class);

        // ensure getContentType() does not return null
        Header header = mock(Header.class);

        HttpEntity entity = mock(HttpEntity.class);
        when(entity.getContentType()).thenReturn(header);
        when(entity.getContent()).thenReturn(inputStream);

        HttpResponse response = mock(HttpResponse.class);
        when(response.getEntity()).thenReturn(entity);

        when(classUnderTest.convertHttpEntityToJson(response)).thenCallRealMethod();

        JSONObject jsonObject = classUnderTest.convertHttpEntityToJson(response);

        Assert.assertTrue("Empty JSON Object was not returned.", jsonObject.isEmpty());;
    }

    @Test
    public void testConvertHttpEntityToJsonNullEntityContentType() {
        HttpEntity entity = mock(HttpEntity.class);
        when(entity.getContentType()).thenReturn(null);

        HttpResponse response = mock(HttpResponse.class);
        when(response.getEntity()).thenReturn(entity);

        when(classUnderTest.convertHttpEntityToJson(response)).thenCallRealMethod();

        JSONObject jsonObject = classUnderTest.convertHttpEntityToJson(response);

        Assert.assertTrue("Empty JSON Object was not returned.", jsonObject.isEmpty());;
    }

    @Test
    public void testConvertHttpEntityToJsonNullEntity() {
        HttpResponse response = mock(HttpResponse.class);
        when(response.getEntity()).thenReturn(null);

        when(classUnderTest.convertHttpEntityToJson(response)).thenCallRealMethod();

        JSONObject jsonObject = classUnderTest.convertHttpEntityToJson(response);

        Assert.assertTrue("Empty JSON Object was not returned.", jsonObject.isEmpty());
    }

}