package com.caruso.mbta.exercise.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.json.JSONObject;

@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class Route {

    @JsonProperty
    private String id;

    @JsonProperty
    private String type;

    @JsonProperty
    private JSONObject links;

    @JsonProperty
    private JSONObject attributes;

    @JsonProperty
    private JSONObject relationships;

    @JsonCreator
    public Route(JSONObject data) {
        this.id = data.getString("id");
        this.type = data.getString("type");
        this.links = data.getJSONObject("links");
        this.attributes = data.getJSONObject("attributes");
        this.relationships = data.getJSONObject("relationships");
    }

}
