package com.caruso.mbta.exercise.api;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.List;

import com.caruso.mbta.exercise.handler.Constants;
import com.caruso.mbta.exercise.handler.HttpClient;
import com.caruso.mbta.exercise.handler.HttpException;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.*;

import com.google.inject.Inject;

import lombok.Getter;
import org.apache.http.impl.io.EmptyInputStream;
import org.apache.http.util.EntityUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;

public class MbtaApiImpl implements MbtaApi, Constants {

    @Getter
    private String baseUri;

    @Inject
    @Getter
    protected HttpClient httpClient;

    @Inject
    public MbtaApiImpl() {
        baseUri = BASE_URL;
    }

    @Override
    public com.caruso.mbta.exercise.handler.HttpResponse invokeGetEndpoint(String uri, String acceptType,
                                                                           String contentType) {
        HttpGet request = getHttpClient().buildGetRequest(uri, acceptType, contentType);

        return invokeBaseRequest("GET", request);
    }


    private com.caruso.mbta.exercise.handler.HttpResponse invokeBaseRequest(String requestType,
                                                                            HttpRequestBase request) {
        try (CloseableHttpResponse response = getHttpClient().execute(request)) {
            return convertApacheHttpResponse(response);
        } catch (IOException e) {
            throw new RuntimeException("Error executing http " + requestType + " request: " + e.getMessage(), e);
        }
    }

    protected String concatUriString(String... strings) {
        StringBuilder buf = new StringBuilder();
        for (String s : strings) {
            buf.append(s + "/");
        }

        // if the last char is a slash need to remove it
        if (buf.toString().endsWith("/")) {
            return buf.toString().substring(0, buf.toString().length() - 1);
        }
        return buf.toString();
    }


    protected String encodeValue(String val) {
        try {
            return URLEncoder.encode(val, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            String msg = "Problem encoding value: " + val + ", msg: " + e.getMessage();
            throw new IllegalArgumentException(msg, e);
        }
    }


    protected com.caruso.mbta.exercise.handler.HttpResponse convertApacheHttpResponse(HttpResponse response){
        return com.caruso.mbta.exercise.handler.HttpResponse.build(response.getStatusLine().getStatusCode(), convertHttpEntityToJson(response));
    }


    protected JSONObject convertHttpEntityToJson(HttpResponse response) {
        try {
            HttpEntity entity = response.getEntity();

            // Return and empty JSONObject for empty content
            if (entity == null)
                return new JSONObject();

            if (entity.getContentType() == null)
                return new JSONObject();

            if (EmptyInputStream.class.isAssignableFrom(response.getEntity().getContent().getClass()))
                return new JSONObject();

            String responseContent = EntityUtils.toString(entity);

            return new JSONObject(responseContent);

        } catch (JSONException e) {
            String msg = "Error parsing http response entity: " + e.getMessage();
            if(response.getStatusLine() == null)
                throw new HttpException(0, "Status Line was null, and " + msg);
            throw new HttpException(response.getStatusLine().getStatusCode(), msg);
        } catch (IOException e) {
            String msg = "Unknown exception encountered while extracting http entity content: " + e.getMessage();
            if(response.getStatusLine() == null)
                throw new HttpException(0, "Status Line was null, and " + msg);
            throw new HttpException(response.getStatusLine().getStatusCode(), msg);
        }
    }

}
