package com.caruso.mbta.exercise.api;

import com.caruso.mbta.exercise.handler.HttpResponse;
import com.caruso.mbta.exercise.model.Route;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class RouteApiImpl extends MbtaApiImpl implements RouteApi {

    @Override
    public List<Route> get() {
        HttpResponse response = invokeGetEndpoint(concatUriString(getBaseUri(), "routes"));

        List<Route> routes = new ArrayList();
        response.getResponseJson().getJSONArray("data").spliterator().forEachRemaining( o -> {
            routes.add( new Route((JSONObject) o));
        });

        return routes;
    }

}
