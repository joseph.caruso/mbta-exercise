package com.caruso.mbta.exercise.api;

import com.caruso.mbta.exercise.model.Route;

import java.util.List;

public interface RouteApi extends MbtaApi {

    List<Route> get();

}
