package com.caruso.mbta.exercise.handler;

public interface Constants {

    public static final int CONNECT_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 10000;

    public static final String BASE_URL = "https://api-v3.mbta.com";

}
