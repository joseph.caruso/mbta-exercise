package com.caruso.mbta.exercise.api;

import com.caruso.mbta.exercise.handler.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.entity.ContentType;
import java.util.List;

public interface MbtaApi {

    HttpResponse invokeGetEndpoint(String uri, String acceptType, String contentType);

    default HttpResponse invokeGetEndpoint(String uri, String contentType) {
        return invokeGetEndpoint(uri, contentType, contentType);
    }

    default HttpResponse invokeGetEndpoint(String uri, ContentType contentType) {
        return invokeGetEndpoint(uri, contentType.getMimeType());
    }

    default HttpResponse invokeGetEndpoint(String uri) {
        return invokeGetEndpoint(uri, ContentType.APPLICATION_JSON);
    }


}
