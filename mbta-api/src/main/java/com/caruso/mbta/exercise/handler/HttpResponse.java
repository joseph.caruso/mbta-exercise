package com.caruso.mbta.exercise.handler;

import lombok.*;
import org.json.JSONObject;

/**
 * Generic class that holds information regarding HTTP invocation responses
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class HttpResponse {

    private int status;
    private JSONObject responseJson;
    private String message;

    public static HttpResponse build(int status, String message) {
        return new HttpResponse(status, null, message);
    }

    public static HttpResponse build(int status, JSONObject jsonEntity) {
        return new HttpResponse(status, jsonEntity, null);
    }

    public static HttpResponse build(int status, Object jsonEntity) {
        if (jsonEntity.getClass().equals(JSONObject.class))
            return new HttpResponse(status, ((JSONObject) jsonEntity), null);
        else if (jsonEntity.getClass().equals(String.class))
            return new HttpResponse(status, null, ((String) jsonEntity));
        throw new RuntimeException("Could not construct HttpResponse for jsonEntity of Class " + jsonEntity.getClass());
    }

}
