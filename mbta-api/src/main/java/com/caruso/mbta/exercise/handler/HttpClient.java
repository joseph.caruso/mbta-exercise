package com.caruso.mbta.exercise.handler;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.net.ssl.SSLContext;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.ssl.SSLContextBuilder;


/**
 * Base helper class that houses common functionality for working with
 * {@link org.apache.http.client.HttpClient}
 */
public class HttpClient implements AutoCloseable {

    public static final String HEADER_CONTENT_TYPE = "Content-type";
    public static final String HEADER_ACCEPT = "Accept";

    CloseableHttpClient client;
    private int DEFAULT_CLIENT_TIMEOUT_MS = 50000;

    public HttpClient() {
        RequestConfig config = RequestConfig.custom().setConnectTimeout(DEFAULT_CLIENT_TIMEOUT_MS)
                .setConnectionRequestTimeout(DEFAULT_CLIENT_TIMEOUT_MS).setSocketTimeout(DEFAULT_CLIENT_TIMEOUT_MS)
                .build();

        client = buildHttpClient(config);
    }

    CloseableHttpClient buildHttpClient(RequestConfig config) {
        return HttpClientBuilder.create().setSSLContext(getSSLContext())
                .setSSLHostnameVerifier(new NoopHostnameVerifier()).setDefaultRequestConfig(config).build();
    }

    public CloseableHttpResponse execute(HttpUriRequest request) throws IOException {
        try {
            return client.execute(request);
        } catch (IOException e) {
            throw new IOException("Error while trying to invoke http endpoint: " + request.getURI().toString()
                    + ", message: " + e.getMessage(), e);
        }
    }

    /**
     * Configures SSL context for a {@link HttpClient}
     * 
     * @return a configured {@link SSLContext}
     */
    SSLContext getSSLContext() {
        try {
            return new SSLContextBuilder().loadTrustMaterial(null, (certificate, authType) -> true).build();
        } catch (Exception e) {
            throw new RuntimeException("Error adding SSL context: " + e.getMessage(), e);
        }
    }

    /**
     * Creates an http GET method with authorization and an accepts the provided
     * media type
     * 
     * @param path       the relative path that is after the "baseURL"
     *
     * @param acceptType the accepted media type
     * 
     * @return a configured {@link HttpGet} object
     */
    public HttpGet buildGetRequest(String path, String acceptType, String contentType) {
        URI uri = null;
        try {
            uri = new URI(path);
            return buildGetRequest(uri, acceptType, contentType);
        } catch (URISyntaxException e) {
            String msg = "Problem creating uri from path: " + path + ", msg: " + e.getMessage();
            throw new IllegalArgumentException(msg, e);
        }
    }


    /**
     * Creates an http GET method with authorization and an accepts
     * "application/json" media type
     *
     * @param path the relative path that is after the "baseURL"
     *
     * @return a configured {@link HttpGet} object
     */
    public HttpGet buildGetRequest(URI path, String acceptType, String contentType) {
        return (HttpGet) buildHttpRequestBase(new HttpGet(path), acceptType, contentType);
    }


    protected HttpRequestBase buildHttpRequestBase(HttpRequestBase base, String acceptType, String contentType) {
        if (acceptType != null)
            base.addHeader(HEADER_ACCEPT, acceptType);
        if (contentType != null)
            base.addHeader(HEADER_CONTENT_TYPE, contentType);
        return base;
    }



    @Override
    public void close() {
        if (client != null) {
            try {
                client.close();
            } catch (IOException e) {
                //log.error("Exception encountered while trying to close the http client: " + e.getMessage(), e);
            }
        }
    }
}