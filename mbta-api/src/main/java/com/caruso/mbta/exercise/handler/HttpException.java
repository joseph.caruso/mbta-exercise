package com.caruso.mbta.exercise.handler;

import lombok.Getter;

/**
 * Exception thrown when there is problems executing and http endpoint
 */
public class HttpException extends RuntimeException {
    private static final long serialVersionUID = -8826476435978095562L;

    @Getter
    private int returnedStatusCode;

    public HttpException(int returnedStatusCode, String message) {
        super(message);
        this.returnedStatusCode = returnedStatusCode;
    }
}
