FROM openjdk:17-slim

COPY execution/target/*jar-with-dependencies.jar execution.jar

ENTRYPOINT ["java", "-jar", "execution.jar"]