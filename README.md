# MBTA Exercise

## Overview
This is a multi-module Java Maven project to show off some basic usages of the MBTA API. 

The link to the documentation for the API is here: https://api-v3.mbta.com/docs/swagger/index.html

The first module is `mbta-api`. Normally this module would just contain a `swagger.json` file, 
and a pom.xml for generating all the client code. However, that was against the rules :)

Instead, a simple REST library was made to interact with a single endpoint. 
A POJO was then built to model the response from this endpoint.

Some unit tests were also built using the Junit and Mockito.

The second module is called `execution`. This module contains the Main class for running the exercises.

Google Guice was used to allow for easy Object instantiation, and Lombok was used to code generation.

The parent pom was designed to construct a "fat jar" which packages up all dependencies and runs the main 
method in Main.class. 

When that jar is run, all exercises will execute.

## Preparation

### Maven
This is a maven project, so you will need some version of maven in order to build this application.

Run `mvn --version` in the terminal to verify. This will also confirm you have your JAVA_HOME set.

### Java
Any version of OpenJDK will work.

### Docker
A Dockerfile has been provided that will be able to execute this exercise in a container. 

IF you dot have Docker you can go to https://www.docker.com/products/docker-desktop to download 
Docker along with the Docker Desktop UI tool.

Run `docker --version` in a terminal to ensure it is set up.

## Building

To build the project simply run `mvn clean package` at the root of the project

This will run all unit tests and create a few jars. The jar called `execution-1.2.3-jar-with-dependencies.jar` 
will run the exercises.

## Running

To run the exercises you must first build the jar, then run `java -jar 
execution/target/execution-1.2.3-jar-with-dependencies.jar` at the root of the project.

You can also run as a Docker container. Once the jar is built, run the following:
```
docker build -t exercise .
docker run -it exercise
```

This application is capable of taking arguments for the final question:
> Write a program that takes 2 stops as an input and determines which commuter rail can be 
> used and presents the long name.


To pass arguments to the jar, do this:
```
java -jar execution/target/execution-1.2.3-jar-with-dependencies.jar Mattapan Ashmont
```
Or for Docker:
```
docker run -it exercise Mattapan Ashmont
```

If your arguments contain spaces, just wrap them in quotes:
```
docker run -it exercise Worcester "South Station"
```





