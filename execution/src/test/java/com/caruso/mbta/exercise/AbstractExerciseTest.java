package com.caruso.mbta.exercise;

import com.caruso.mbta.exercise.exercises.AbstractExercise;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AbstractExerciseTest {

    private AbstractExercise classUnderTest = mock(AbstractExercise.class);

    private ObjectMapper objectMapper = new ObjectMapper();;

    @Test
    public void testConvertJSONArrayIntoListStrings() {
        JSONArray array = new JSONArray();
        array.put("hello");
        array.put("world");

        classUnderTest.mapper = objectMapper;
        when(classUnderTest.convertJSONArrayIntoList(array)).thenCallRealMethod();

        List<String> list = classUnderTest.convertJSONArrayIntoList(array);

        Assert.assertEquals("hello", list.get(0));
        Assert.assertEquals("world", list.get(1));
    }

    @Test
    public void testConvertJSONArrayIntoListIntegers() {
        JSONArray array = new JSONArray();
        array.put(Integer.valueOf(1));
        array.put(Integer.valueOf(2));

        classUnderTest.mapper = objectMapper;
        when(classUnderTest.convertJSONArrayIntoList(array)).thenCallRealMethod();

        List<Integer> list = classUnderTest.convertJSONArrayIntoList(array);

        Assert.assertEquals(Integer.valueOf(1), list.get(0));
        Assert.assertEquals(Integer.valueOf(2), list.get(1));
    }

    @Test
    public void testConvertJSONArrayIntoListRandoms() {
        JSONArray array = new JSONArray();
        array.put(Integer.valueOf(1));
        array.put("world");

        classUnderTest.mapper = objectMapper;
        when(classUnderTest.convertJSONArrayIntoList(array)).thenCallRealMethod();

        List<JSONObject> list = classUnderTest.convertJSONArrayIntoList(array);

        Assert.assertEquals(Integer.valueOf(1), list.get(0));
        Assert.assertEquals("world", list.get(1));
    }
}