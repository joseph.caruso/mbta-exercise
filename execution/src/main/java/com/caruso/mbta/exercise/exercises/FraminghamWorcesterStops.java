package com.caruso.mbta.exercise.exercises;

import com.caruso.mbta.exercise.model.Route;

public class FraminghamWorcesterStops extends AbstractExercise {

    private static final String FW_LINE = "Framingham/Worcester Line";

    public void run(){
        System.out.println("Printing the stops at " + FW_LINE);
        for(Route route : routeApi.get()){
            if(FW_LINE.equals(getLongName(route)))
                getStops(route).forEach( stop -> {
                    System.out.println(stop);
                });
        }
    }

}
