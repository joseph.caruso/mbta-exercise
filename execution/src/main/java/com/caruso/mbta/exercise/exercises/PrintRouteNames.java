package com.caruso.mbta.exercise.exercises;

import com.caruso.mbta.exercise.model.Route;

public class PrintRouteNames extends AbstractExercise {

    public void run(){
        for(Route route : routeApi.get()){
            System.out.println(getLongName(route));
        }
    }

}
