package com.caruso.mbta.exercise;

public class Main {

    public static void main(String[] args) {
        Exercise exercise = new Exercise();
        exercise.run(args);
    }

}