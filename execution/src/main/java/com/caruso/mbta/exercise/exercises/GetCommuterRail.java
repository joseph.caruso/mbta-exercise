package com.caruso.mbta.exercise.exercises;

import com.caruso.mbta.exercise.model.Route;

import java.util.List;

public class GetCommuterRail extends AbstractExercise {

    public void run(String start, String stop)  {
        List<Route> routes = routeApi.get();
        boolean hasStart = false, hasStop = false;
        Route validRoute = null;

        for(Route route : routes){
            List<String> stops = convertJSONArrayIntoList(getStops(route));
            if(stops.contains(start))
                hasStart = true;
            if (stops.contains(stop))
                hasStop = true;

            if (hasStart && hasStop){
                validRoute = route;
                break;
            }
        }

        if (validRoute != null) {
            System.out.println("Found a route. Use " + getLongName(validRoute));
        } else {
            System.out.println("No route was found for " + start + " and " + stop);
        }

    }

}
