package com.caruso.mbta.exercise.utils;

import com.caruso.mbta.exercise.api.MbtaApi;
import com.caruso.mbta.exercise.api.MbtaApiImpl;
import com.caruso.mbta.exercise.api.RouteApi;
import com.caruso.mbta.exercise.api.RouteApiImpl;
import com.caruso.mbta.exercise.exercises.FraminghamWorcesterStops;
import com.caruso.mbta.exercise.exercises.GetCommuterRail;
import com.caruso.mbta.exercise.exercises.PrintRouteNames;
import com.caruso.mbta.exercise.handler.HttpClient;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.AbstractModule;

public class GuiceModule extends AbstractModule {

    @Override
    protected void configure() {
        // Exercises
        bind(PrintRouteNames.class);
        bind(FraminghamWorcesterStops.class);
        bind(GetCommuterRail.class);

        // Api Classes
        bind(HttpClient.class);
        bind(MbtaApi.class).to(MbtaApiImpl.class);
        bind(RouteApi.class).to(RouteApiImpl.class);

        // Utility classes
        bind(ObjectMapper.class);

    }

}

