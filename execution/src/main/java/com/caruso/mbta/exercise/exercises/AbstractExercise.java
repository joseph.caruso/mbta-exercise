package com.caruso.mbta.exercise.exercises;

import com.caruso.mbta.exercise.api.RouteApi;
import com.caruso.mbta.exercise.model.Route;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import lombok.Setter;
import org.json.JSONArray;

import java.util.List;

public abstract class AbstractExercise {

    @Inject
    public RouteApi routeApi;

    @Inject
    public ObjectMapper mapper;

    public static final String LONG_NAME = "long_name";
    public static final String DIRECTION_DESTINATIONS = "direction_destinations";

    public String getLongName(Route route){
        return route.getAttributes().getString(LONG_NAME);
    }

    public JSONArray getStops(Route route){
        return route.getAttributes().getJSONArray(DIRECTION_DESTINATIONS);
    }

    public <T> List<T> convertJSONArrayIntoList(JSONArray array){
        try {
            List<T> x = mapper.readValue(array.toString(), List.class);
            return x;
        }catch (JsonProcessingException e) {
            throw new RuntimeException("Could not convert the JSONArray into a List");
        }
    }

}
