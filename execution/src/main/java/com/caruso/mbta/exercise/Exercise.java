package com.caruso.mbta.exercise;

import com.caruso.mbta.exercise.exercises.FraminghamWorcesterStops;
import com.caruso.mbta.exercise.exercises.GetCommuterRail;
import com.caruso.mbta.exercise.utils.GuiceModule;
import com.caruso.mbta.exercise.exercises.PrintRouteNames;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;

import java.util.Arrays;

public class Exercise {

    @Inject
    public PrintRouteNames printRouteNames;

    @Inject
    public FraminghamWorcesterStops framinghamWorcesterStops;

    @Inject
    public GetCommuterRail getCommuterRail;

    private static final String start = "Boston College";
    private static final String stop = "Park Street";

    protected Injector injector;

    public Exercise(){
        injector = initializeInjector();
        injector.injectMembers(this);
    }

    public void run(String[] args){
        System.out.println("Question 1");
        printRouteNames.run();

        System.out.println("Question 2");
        framinghamWorcesterStops.run();

        System.out.println("Question 3");
        if(args.length == 2)
            getCommuterRail.run(args[0], args[1]);
        else {
            System.out.println("Exactly 2 arguments was not provided. Using default arguments: " + start + " and " + stop);
            getCommuterRail.run(start, stop);
        }
    }


    private Injector initializeInjector() {
        return Guice.createInjector(Arrays.asList(new GuiceModule()));
    }

}
